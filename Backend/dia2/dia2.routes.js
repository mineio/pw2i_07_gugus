import { Router } from 'express';
import { getAlldatadia2, loadDia2data } from './dia2.controller.js';
  
const routerDia2 = Router();

routerDia2.get('/', getAlldatadia2);
 
export { routerDia2 } 