import axios from "axios";
import { getAllDia2 } from "./dia2.model.js";
import { response } from "express";

let Dia2All = [];

//Funktion um die Daten von der API zu laden
function loadDia2data() {
  return axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-58/exports/json")
    .then(async (response) => {
      const datas = response.data;
      //Erstellung Objekte für Karte und Diagramme
      for (let i = 0; i < datas.length; i++) {
        const daten = datas[i];
        const Dia2 = {
          Jahr: daten.jahr,
          GeschlechtCode: daten.geschlecht_code,
          Geschlecht: daten.geschlecht_bezeichnung,
          AnzahlPersonen: daten.anzahl_personen,
        };


        Dia2All.push(Dia2);
      }
    });
}
//function um die Daten ans Frontend zu schicken
async function getAlldatadia2(request, response) {
  const datas = await getAllDia2();
  response.json(Dia2All);
}

export { loadDia2data, getAlldatadia2 };
