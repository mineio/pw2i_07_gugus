import axios from "axios";
import { getAllDia1 } from "./dia1.model.js";
import { response } from "express";

let Dia1All = [];

//Funktion um die Daten von der API zu laden
function loadDia1data() {
  return axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-58/exports/json")
    .then(async (response) => {
      const datas = response.data;
      //Erstellung Objekte für Karte und Diagramme
      for (let i = 0; i < datas.length; i++) {
        const datensatz = datas[i];
        const Dia1 = {
          GemeindeID: datensatz.bfs_nr_gemeinde,
          Jahr: datensatz.jahr,
          AltersklasseID: datensatz.alter5klassen_code,
          AlterklasseBezeichnung: datensatz.alter5klassen_bezeichnung,
          AnzahlPersonen: datensatz.anzahl_personen,
        };

        Dia1All.push(Dia1);
      }
    });
}
//function um die Daten ans Frontend zu schicken
async function getAlldatadia1(request, response) {
  const datas = await getAllDia1();
  response.json(Dia1All);
}

export { loadDia1data, getAlldatadia1 };
