import { Router } from 'express';
import { getAlldatadia1, loadDia1data } from './dia1.controller.js';
  
const routerDia1 = Router();

routerDia1.get('/', getAlldatadia1);

export { routerDia1 } 