import axios from "axios";
import { getAllKarteDia3 } from "./KarteDia3.model.js";
import { response } from "express";

let KarteDia3All = [];

//Funktion um die Daten von der API zu laden
function loadKarteDia3data() {
  return axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-58/exports/json")
    .then(async (response) => {
      const datas = response.data;
      //Erstellung Objekte für Karte und Diagramme
      for (let i = 0; i < datas.length; i++) {
        const daten = datas[i];
        const KarteDia3 = {
          GemeindeID: daten.bfs_nr_gemeinde,
          GemeindeName: daten.gemeinde_name,
          Jahr: daten.jahr,
          AnzahlPersonen: daten.anzahl_personen,
        };
        KarteDia3All.push(KarteDia3);
      }
    });
}
//function um die Daten ans Frontend zu schicken
async function getAlldataKarteDia3(request, response) {
  const datas = await getAllKarteDia3();
  response.json(KarteDia3All);
}

export { loadKarteDia3data, getAlldataKarteDia3 };
