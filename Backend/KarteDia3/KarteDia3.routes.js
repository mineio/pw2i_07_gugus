import { Router } from 'express';
import { getAlldataKarteDia3, loadKarteDia3data } from './KarteDia3.controller.js';
  
const routerKarteDia3 = Router();

routerKarteDia3.get('/', getAlldataKarteDia3);
 
export { routerKarteDia3 } 