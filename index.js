import express from "express";
import { loadKarteDia3data } from "./Backend/KarteDia3/KarteDia3.controller.js";
import { loadDia1data } from "./Backend/dia1/dia1.controller.js";
import { loadDia2data } from "./Backend/dia2/dia2.controller.js";
import { routerKarteDia3 as KarteDia3Router } from "./Backend/KarteDia3/KarteDia3.routes.js";
import { routerDia1 as dia1Router } from "./Backend/dia1/dia1.routes.js";
import { routerDia2 as dia2Router } from "./Backend/dia2/dia2.routes.js";

const app = express();

app.use(express.static("frontend"));

app.use(express.json());
app.use("/api/KarteDia3", KarteDia3Router);
app.use("/api/dia1", dia1Router);
app.use("/api/dia2", dia2Router);

await loadKarteDia3data();
await loadDia1data();
await loadDia2data();

app.listen(3001, async () => {
  console.log("Server listens to http://localhost:3001");
});
