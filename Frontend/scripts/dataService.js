function getAlldia1() {
  const request = axios.get('/api/dia1');
  return request.then((response) => response.data);
}
function getAlldia2() {
  const request = axios.get('/api/dia2');
  return request.then((response) => response.data);
}
function getAllKarteDia3() {
  const request = axios.get('/api/KarteDia3');
  return request.then((response) => response.data);
}

export  { getAlldia1,getAlldia2,getAllKarteDia3 };
 